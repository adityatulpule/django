
from django.shortcuts import render , redirect , render_to_response
from django.http import HttpResponse
from .models import Posts
#from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.views.generic.base import TemplateView

from django.http import HttpRequest

from .forms import Home_Form_model
from .forms import Create_User_Form
from .forms import LoginForm

from .models import User
from django.template.response import TemplateResponse
from django.contrib import messages

import pylab
from pylab import *
import PIL, PIL.Image
import matplotlib.pyplot as plt
import xlrd
from io import BytesIO
import folium


import pandas as pd
from pandas import DataFrame as df
import mplleaflet

import IPython
from IPython.display import HTML



def  main_page(request):

    """     this is the Home page     """




    context = {


    'page_count': request.session['page_count'] + 1


    }



    print('Path info : '+request.path_info)
    print ()
    print(request.session)

    return render(request,'posts/main_page.html',context)




def  current_posts(request):

    """     this page will display the Current Posts    """



    all_posts = Posts.objects.all()[:10]


    context = {

    'title':'Latest Posts',
    'posts':all_posts,
    'page_count': request.session['page_count'] + 1


    }



    print('Path info : '+request.path_info)
    print ()
    print(request.session)

    return render(request,'posts/current_posts.html',context)









def index(request) :

    """     this is the index page     """

    all_posts = Posts.objects.all()[:10]


    context = {

    'title':'Latest Posts',
    'posts':all_posts,
    'page_count': request.session['page_count'] + 1


    }

    #return HttpResponse('Hello Ady u r in Posts')

    print('Path info : '+request.path_info)
    print ()
    print(request.session)

    return render(request,'posts/index.html',context)
    #return render(request,'posts/base.html')




def details(request , id):

    """     this is the details page     """

    post = Posts.objects.get(id = id)

    context = {

            'post':post

    }

    return render(request,'posts/details.html',context)


def slider(request):

    """     this is the slider page     """


##    print('{}\r\n{}\r\n\r\n{}'.format(
##        '-----------START-----------',
##        request.method + ' ' +
##        '\r\n'.join('{}: {}'.format(k, v) for k, v in request.headers.items()),
##        request.body,
##    ))


    print ([request])
    return render(request, 'posts/slider.html')



def earth_3D(request):



##    latitudes = [46.87,30.87,40.6337,]
##    longitudes = [-102.7898,-102.7898,-74.4074]
##    locations = ['North Dakota','Texas','New York']
##
##
##    df = ({'latitudes':latitudes,'longitudes':longitudes,'locations':locations})
##
##    print (df['longitudes'])
##
##
##
##
##    line_plot_figure , line_plot_ax =  plt.subplots(figsize=(12,9))
##    line_plot_ax.plot(df['longitudes'],df['latitudes'],'orange',linewidth=7)
##    line_plot_ax.plot(df['longitudes'],df['latitudes'],'ks')

    #plt.show()
    # m = mplleaflet.display(fig = line_plot_figure)
    #map_obj = m.save('A:\django\DjangoProject_1\posts\templates\posts\earth2.html')



    points = [(46.87,-102.7898),(30.87,-102.7898),(40.6337, -74.4074)]
    m = folium.Map(location = [46.87,-102.7898],zoom_start=4)
    folium.Marker(location = [30.87,-102.7898],popup="Texas",tooltip = "Location information").add_to(m)
    folium.Marker(location = [46.87,-102.7898],popup="North Dakota",tooltip = "Restaurant",icon = (folium.Icon(icon='utensils',prefix='fa'))).add_to(m)
    folium.Marker(location=[40.6337, -74.4074], popup= 'New York',icon=folium.Icon(color='red', icon='home', prefix='fa')).add_to(m)
    folium.PolyLine(points, color="orange", weight=2.5, opacity=1).add_to(m)

    m.save('A:/django/DjangoProject_1/posts/templates/posts/earth3.html')


    return render(request,'posts/earth3.html')






def data_sci_1(request):

        """     This function prints graph output on the web page       """

##    workbook = xlrd.open_workbook('A:\Sem_4\Data Mining\project2\\table-1.xls')
##    #print(workbook)
##    worksheet = workbook.sheet_by_index(0)
##
##    total_rows = worksheet.nrows
##    total_cols = worksheet.ncols
##
##    table = []
##    record = []
##
##
##    for x in range(3,24):
##        for y in range(total_cols):
##            record.append(worksheet.cell(x,y).value)
##        table.append(record)
##        record = []
##        x += 1

    #print (table[0][0])   # only year

    #print (table)

        year = ['2000','2001','2002','2003']
        crime1 = [2,5,6,9]
        crime2 = [1,1,5,9]

        #for i in table:
            #print (i[0],'\t',i[5],'\t',i[6])

    ##        year.append(str(i[0]))
    ##        crime1.append(str(i[5]))
    ##        crime2.append(str(i[6]))
        #print (year)

        plt.rcParams['figure.figsize']= (15,6)
        plt.bar(year,crime1,width = 0.9)

        plt.title(" Crimes PER Year ")
        plt.xlabel('Crimes ')
        plt.ylabel('Year')

        #plt1 = plt.show()

        plt.rcParams['figure.figsize'] = (15,8)
        plt.scatter(year,crime1,label = ' Goods / day ')
        plt.scatter(year,crime2,label = ' Price for goods / day ')
        #plt2 = plt.show()



        # These 6 lines of code prints image on webpage using given format (eg: 'PNG')
        buffer = BytesIO()
        canvas = pylab.get_current_fig_manager().canvas
        canvas.draw()
        pilImage = PIL.Image.frombytes("RGB", canvas.get_width_height(), canvas.tostring_rgb())

        print (str(buffer))
        pilImage.save(buffer, "PNG")
        pylab.close()


        #buffer.getvalue() has bytes value , hence BytesIO() needed
        # Send buffer in a http response the the browser with the mime type image/png set
        return HttpResponse(buffer.getvalue(), content_type="image/png")



##        context = {
##
##        'plt1':plt1,
##        'plt2':plt2
##
##        }
##
##
##
##        return render(request , 'posts/data_sci_1.html', context)











def my_form(request):

    """     this is the sample form method     """

    print(request.method)

    if request.method == 'POST':

        form = Home_Form_model(request.POST)

        if form.is_valid():

            cd = form.cleaned_data
            print(cd)
    else:

        form = Home_Form_model()

    return render(request, "posts/forms.html", {'form': form})




class login(TemplateView):


    """     this is the Login page (posts/login)     """

    def get(self, request, *args, **kwargs):

        form = LoginForm()

        if request.session.has_key('username'): # this is for refreshing the session

            del request.session['username']

        return render(request, 'posts/login.html', {'form': form})



    def post(self, request, *args, **kwargs):



        if request.method == 'POST':

            form = LoginForm(request.POST)
            context = {

                'form': form

                            }

            if form.is_valid():

                try:

                    form_data = form.save()
                    last_login = request.user.last_login # this is to access auth_user variables

                    username = form_data.newuser_name   #.cleaned_data['newuser_name']
                    request.session['username'] = username


                    context = {

                        'form':form,
                        'username': username,
                        'last_login':last_login

                    }

                    return render(request , 'posts/login.html', context)

                except Exception as e:



                    print('views Exception block ')
                    return render(request , 'posts/login.html', {'error':'Excepiton generated !'})

        return render(request , 'posts/login.html', context)










class FormHomeClass(FormView):


    """     this is the sample class     """

    def get(self, request, *args, **kwargs):

        form = Home_Form_model()
        return render(request, 'posts/forms.html', {'form': form})


    def post(self, request, *args, **kwargs):

        form = Home_Form_model(data=request.POST)

        if form.is_valid():

            print(form.cleaned_data)


            form = Home_Form_model()

            return render(request, 'posts/forms.html', {'form': form})

        return render(request, 'posts/forms.html', {'form': form})








class Form_Home_class(TemplateView):

    """     this is the Form page class     """


    template_name = 'posts/forms.html'


    def get(self, request, *args, **kwargs):

        form = Home_Form_model()

        return render(request, 'posts/forms.html', {'form': form})


    def post(self, request, *args, **kwargs):

        form = Home_Form_model(request.POST)
        context = {

                'form': form

                            }

        if form.is_valid():

            print(request.POST)

            try:


                order_by_email = User.objects.order_by('email')
                all_users = User.objects.all()
                filter_obj = User.objects.filter(username = 'ady2')

                form_data = form.save()
                print(all_users)
                #print(form_data.id,form_data.body)

##                data = Posts.objects.get(id=5)
##                print (data.title)
##
                context = {

                'form': form,
                'data': form_data,
                'all_users': all_users,
                'filter_obj': filter_obj,
                'order_by_email':order_by_email,



                            }


                #return render(request, 'posts/forms.html', context)

                # message.(DEBUG , SUCCESS , WARNING ,ERROR) other possibilities
                messages.add_message(request, messages.INFO, 'This data is stored in the database', extra_tags='dragonball')


                return redirect(request, 'posts/forms.html', context)

                form.save()

            except Exception as e:

                    data = Posts.objects.get(id=5) # in case of exception print this
                    context['data'] = data
                    #print (data.title)


                    print('this is excepiton block.... '+str(e))

        return render(request, 'posts/forms.html', context)


    def middleware_process_function(request):

        args = {'user':request.user}
        return render(request , 'posts/forms.html' , args)





class Create_New_User(TemplateView):


    """     this is the New User Creation page class     """


    template_name = 'posts/create_user.html'


    def get(self, request, *args, **kwargs):

        form = Create_User_Form()

        context = {'form':form}



        return render(request, 'posts/create_user.html', {'form': form})


    def post(self, request, *args, **kwargs):

        form = Create_User_Form(request.POST)
        context = {

                'form': form

                            }

        if form.is_valid():

            #print(request.POST)

            try:

                form_data = form.save()


                #form_data = Posts(newuser_name = form.cleaned_data['newuser_name'],
                 #                 newuser_password = form.cleaned_data['newuser_password'])

                #newuser_name.save()
                #newuser_password.save()

                #form_data.save()
                print('---->'+form_data.newuser_name)

##                data = Posts.objects.get(id=5)
##                print (data.title)
##
                context = {

                'form': form,
                'data': form_data


                            }


                #return render(request, 'posts/create_user.html', context)
                return TemplateResponse(request , 'posts/Success_msg.html',context) # using the template response ,
                                                                                    # just to give a call to the middleware method (otherwise use render)



                #form.save()

            except Exception as e:

                    data = Posts.objects.get(id=5) # in case of exception print this
                    context['data'] = data
                    print (e)
                    #print (data.title)


                    print('this is excepiton block.... '+str(e))

        return render(request, 'posts/create_user.html', context)
