from datetime import datetime
from django.shortcuts import render, redirect
from django.urls import resolve
from django.http import HttpResponse
from django.template.response import TemplateResponse



class TestMiddleware(object):

    def __init__(self, get_response):

         # """     this is the middleware constructor method     """

    	self.get_response = get_response

    def __call__(self, request):

        """     this is the middleware constructor method     """

        print (" You are inside middleware call ")


        list_1 = ['admin/login/']
        request_path = request.path_info.lstrip('/')
        print (request_path)

        if not request.user.is_authenticated and not request_path in list_1:

            print('authentication condition in call')

            return redirect('/admin/login/?next=/admin/')

            #return render(request,'posts/index.html') # renders without access


        request.timestamp = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        response = self.get_response(request)
        # print(response)
        myfunc, myargs, mykwargs = resolve("/posts/")
        mymodule = myfunc.__name__
        current_page_url = request.path_info.lstrip('/')
        # print(current_page_url)
        # print(request.user.is_staff)
        # print(not (current_page_url in ['posts/'] and mymodule == 'index'))
        # print(not request.user.is_staff or not (current_page_url in ['posts/'] and mymodule == 'index'))



        # print(current_page_url)
        if "current_page_url" in request.session:
            if current_page_url in request.session['current_page_url']:
                request.session['current_page_url'][current_page_url] += 1
            else:
                request.session['current_page_url'][current_page_url] = 0
        else:
            request.session['current_page_url'] = {}
            request.session['current_page_url'][current_page_url] = 0

        if 'page_count' in request.session:
            request.session['page_count'] += 1
        else:
            request.session['page_count'] = 0

        print(request.session['current_page_url'])

        if request.session['page_count'] > 5:
            pass
        	#return HttpResponseRedirect('posts/')

        return response



##    def process_response(self, request, response):
##        print('process_response')
##        print(request)
##        current_page_url = request.path_info.lstrip('/')
##        print(current_page_url)
##        print(response)
##
##        if not request.user.is_staff:
##            print(current_page_url)
##            # print(mymodule)
##            # print(not (current_page_url in ['posts/', 'posts/index'] and mymodule == 'index'))
##            # print('hhhh')
##            exempt = ['/admin/login/?next=/admin/', '/posts/slider','/posts/forms.html','posts/index','posts/']
##            if not (current_page_url in exempt):
##                print('here')
##                return redirect("index")
##            elif 'posts/' == current_page_url:
##                pass
##            else:
##                return redirect(current_page_url)
##        return response
##
##    def process_request(self, request):
##    	print ("process_request")
##    	request._request_time = datetime.now()

    def process_template_response(self, request, response):

        """     this is a middleware method called using Templateresponse()     """

        print('process_template_response')
        print (request.session)
##        print(request.user)
##        list_1 = ['admin/login/']
##        request_path = request.path_info.lstrip('/')
##        print (request_path)
##
##        if not request.user.is_authenticated and not request_path in list_1:
##
##
##
##        #if not request.user.is_authenticated :
##
##            print ('This is if stsment')
##            return TemplateResponse(request , 'posts/index.html')

    	# response_time = request._request_time - datetime.now()
    	#response.context_data['response_time'] = abs(response_time)



        return response
        #return TemplateResponse(request , 'posts/Success_msg.html')


    def process_exception(self, request, exception):

         #"""     this is the middleware exception method     """

    	print('process_exception')
    	# print (exception.__class__.__name__)
    	# print (exception.message)
    	return None