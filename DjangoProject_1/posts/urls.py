from django.conf.urls import url
from . import views

urlpatterns = [


        url(r'^details/(?P<id>\d)/$',views.details, name = 'details'),
        url(r'^slider/$',views.slider, name='slider.html'),


        url(r'^forms.html/$',views.Form_Home_class.as_view(), name='forms.html'),
        url(r'^forms1.html/$',views.my_form, name='forms1.html'),
        url(r'^forms2.html/$',views.FormHomeClass.as_view(), name='forms2.html'),
        url(r'^create_user.html/$',views.Create_New_User.as_view(), name='create_user.html'),
        url(r'^login.html/$',views.login.as_view(), name = 'login.html'),
        url(r'^data_sci_1.html/$',views.data_sci_1, name = 'data_sci_1.html'),
        url(r'^current_posts.html/$',views.current_posts, name = 'current_posts.html'),
        url(r'^index.html/$',views.index, name = 'index.html'),
        url(r'^earth.html/$',views.earth_3D, name = 'earth3.html'),

        url(r'^main_page.html/$',views.main_page, name = 'main_page'),



        url(r'^$',views.main_page, name = 'main_page.html'),






]